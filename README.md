# Spring Boot Login, Registration and Confirmation Email 


Let's try changing the expire time. <br> 
The sql code we need to write (or you can wait)
```roomsql
    update confirmation_token set expires_at = now() - interval '1 day' where id = 1;
```

```postgresql

registration=# select * from confirmation_token;
        -[ RECORD 1 ]+-------------------------------------
        id           | 1
        confirmed_at |
        created_at   | 2021-04-29 20:11:21.975471
        expires_at   | 2021-04-29 20:26:21.976468
        token        | 4ca34fd4-5129-4486-a21a-d929c3c71d9a
        app_user_id  | 1


        registration=# update confirmation_token set expires_at = now() - interval '1 day' where id = 1;
        UPDATE 1
        registration=# select * from confirmation_token;
        -[ RECORD 1 ]+-------------------------------------
        id           | 1
        confirmed_at |
        created_at   | 2021-04-29 20:11:21.975471
        expires_at   | 2021-04-28 20:14:52.13
        token        | 4ca34fd4-5129-4486-a21a-d929c3c71d9a
        app_user_id  | 1


        registration=#

```

Postman result after expire time change.

```json

{
    "timestamp": "2021-04-29T19:15:07.625+00:00",
    "status": 500,
    "error": "Internal Server Error",
    "message": "token expired",
    "path": "/api/v1/registration/confirm"
}

```

<details><summary>author's details...</summary>
<p>

```
     Sebahattin KALACH
        Software Artisan
```

</p>
</details>
